import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import { createPinia } from 'pinia'
import { lazyPlugin } from '@/directives'
import router from './router'

//引入初始化的样式文件
import '@/styles/common.scss'

// import { getCategory } from './apis/testAPI'

// getCategory().then(res=>{
//     console.log(res)
// })
const app = createApp(App)
const pinia = createPinia()
app.use(ElementPlus)
app.use(router)
app.use(pinia)
app.use(lazyPlugin)
app.mount('#app')



