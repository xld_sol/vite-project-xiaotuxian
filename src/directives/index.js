//定义懒加载插件
import { useIntersectionObserver } from '@vueuse/core'
export const lazyPlugin = {
    install(app) {
        app.directive('img-lazy', {
            mounted(el, blinding) {
                const { stop } = useIntersectionObserver(
                    // console.log(el,blinding.value),
                    el,
                    ([{ isIntersecting }]) => {
                        if (isIntersecting) {
                            // console.log(el.src),
                            el.src = blinding.value
                            stop()
                        }

                    },
                )
            }
        })
    }
}