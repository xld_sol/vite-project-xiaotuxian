import httpInstance from "@/utils/http";

export function getGategoryAPI() {
    return httpInstance({
        url:'/home/category/head'
    })
}