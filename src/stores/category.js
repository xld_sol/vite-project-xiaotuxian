// stores/counter.js
import { defineStore } from 'pinia'
import { getGategoryAPI } from '@/apis/layout.js';
import { onMounted, ref } from 'vue';




export const useCategoryStore = defineStore('category', () => {
    const CategoryList = ref([])


    //导航列表数据
    const getCategory = async () => {
        await getGategoryAPI().then((res) => {
            CategoryList.value = res.result
        })
    }

    return {
        CategoryList,
        getCategory
    }

})