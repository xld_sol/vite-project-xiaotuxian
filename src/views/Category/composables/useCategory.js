//封装category的业务代码
import { getCategoryAPI } from '@/apis/catergory'
import {  ref } from 'vue';
import { onBeforeRouteUpdate, useRoute } from 'vue-router';
export function useCategory() {
    //获取数据
    const categoryData = ref({})
    const route = useRoute()
    const getCategory = async (id = route.params.id) => {
        await getCategoryAPI(id).then((res) => {
            categoryData.value = res.result
        })
    }

    onBeforeRouteUpdate((to) => {
        getCategory(to.params.id)
    })
    return{
        categoryData
    }
}