//封装轮播图的业务代码
import { ref ,onMounted} from 'vue'
import { getBannerAPI } from '@/apis/home';
export function useBanner() {
    const bannerList = ref([])

    const getBannerData = async () => {
        await getBannerAPI({ distributionSite: '2' }).then((res) => {
            bannerList.value = res.result
        })
    }
    onMounted(() => {
        getBannerData()
    })


    return {
        bannerList
    }
}